# major.minor.patch.release.number
# release must be one of alpha, beta, rc or final
VERSION = (0, 4, 1, 'alpha', 13)

__version__ = '.'.join(str(x) for x in VERSION)
