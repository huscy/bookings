from django.urls import include, path
from rest_framework.routers import DefaultRouter
from rest_framework_nested.routers import NestedDefaultRouter

from huscy.bookings import views
from huscy.project_design.urls import project_router

router = DefaultRouter()
router.register('timeslots', views.TimeslotViewSet)

project_router.register('timeslots', views.ListTimeslotsViewSet, basename='project-timeslots')

experiment_router = NestedDefaultRouter(project_router, 'experiments', lookup='experiment')
experiment_router.register('timeslots', views.ListTimeslotsViewSet, basename='experiment-timeslots')


urlpatterns = [
    path('api/', include(router.urls + project_router.urls + experiment_router.urls)),
]
